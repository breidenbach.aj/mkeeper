#!/usr/bin/env ruby
require 'discordrb'
require 'optparse'
require 'shellwords'
require 'json/ext'
require 'net/http'
require 'uri'
require_relative '../lib/bot/foo'

TOKEN = ENV['BOT_TOKEN']
CHANNEL = ENV['BOT_CHANNEL']

bot = Discordrb::Bot.new token: TOKEN

File.open('db/discord') do |file|
  @processed_messages = file.readlines.map(&:to_i)
end

def record_processed_message(message)
  @processed_messages << message.id
  File.open('db/discord', 'a') do |file|
    file.write("#{message.id}\n")
  end
end

def split_args(content)
  content.split('-').map do |s|
    (flag, value) = s.split(/\s+/, 2)

    next [] unless value

    ["-#{flag}", value.shellsplit.join(' ')]
  end.flatten
end

def process_args(content)
  options = {}
  opts = OptionParser.new
  opts.on('-p <place>', '--place <place>', String) { |place| options[:store] = place }
  opts.on('-a <amount>', '--amount <amount>', Integer) { |amount| options[:price] = amount }
  opts.on('-c <category>', '--category <category>', String) { |category| options[:category] = category }

  args = split_args(content)

  args = opts.order!(args) {}
  opts.parse! args

  options
end

def process_message(message)
  return if @processed_messages.include? message.id

  options = { price: 0, created_at: message.timestamp, category: 'None' }

  options.merge!(process_args(message.content))

  puts options.to_json

  url = URI("http://localhost:#{ENV['PORT'] || '3000'}/purchase")
  response = Net::HTTP.post(url, options.to_json,
                            { 'content-type': 'application/json' })

  unless response.is_a? Net::HTTPSuccess
    warn 'Failed to post message, aborting'
    exit 1
  end
  record_processed_message(message)
end

bot.message do |event|
  next unless event.is_a? Discordrb::Events::MessageEvent

  process_message(event.message)
end

bot.ready do |_event|
  all_channels = bot.servers.values.map do |s|
    s.is_a?(Discordrb::Server) ? s.channels : []
  end.flatten

  channel = all_channels.find { |c| c.id == CHANNEL.to_i }

  if channel.nil?
    warn 'Channel not found, aborting'
    exit 1
  end

  before_id = nil

  puts @processed_messages.to_json
  loop do
    history = channel.history(10, before_id)

    break if history.empty?

    break if channel.history(10, before_id).any? do |message|
      before_id = message.id

      next true if @processed_messages.include? message.id

      process_message(message)
      puts message.inspect
    end
  end
end

bot.run
