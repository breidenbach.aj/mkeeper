class DefaultForCategory < ActiveRecord::Migration[7.0]
  def change
    change_column :purchases, :price, :integer, default: 0, null: false
    change_column :purchases, :category, :string, default: 'None', null: false
    change_column :purchases, :store, :string, null: false
  end
end
