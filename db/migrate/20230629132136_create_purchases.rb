class CreatePurchases < ActiveRecord::Migration[7.0]
  def change
    create_table :purchases do |t|
      t.string :store
      t.integer :price
      t.string :category

      t.timestamps
    end
  end
end
