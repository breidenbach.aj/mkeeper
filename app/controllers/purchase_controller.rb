require 'digest/sha1'
class PurchaseController < ApplicationController
  def index_to_hsl(index)
    "hsl(#{index * 150 % 256}, #{80 - index * 2}%, 40%)"
  end

  def created_after
    (year, month) = if params[:month]
                      params[:month].split('-')
                    else
                      [Time.now.year, Time.now.month]
                    end

    Time.new(year, month)
  end

  def created_before
    after = created_after

    (year, month) = if after.month < 12
                      [after.year, after.month + 1]
                    else
                      [after.year + 1, 1]
                    end

    Time.new(year, month)
  end

  def index
    @purchases = Purchase.select('SUM(price) as price',
                                 'category').group(:category).where(created_at: created_after...created_before)
    @purchases = @purchases.sort_by { |p| - p.price }
    @data = @purchases.each_with_object({}) do |e, h|
      h[e.category] = e.price
    end

    @colors = @data.map.with_index { |_k, i| index_to_hsl(i) }
  end

  def show
    @category = params[:category]

    @show_all = @category == '*'

    @purchases = if @show_all
                   Purchase.order(:created_at).where(created_at: created_after...created_before).all
                 else
                   Purchase.order(:created_at).where(category: @category,
                                                     created_at: created_after...created_before).all
                 end
  end

  def create
    purchase = Purchase.new(
      price: params[:price].to_i,
      store: params[:store],
      category: params[:category]
    )

    purchase.created_at = Time.parse(params[:created_at]) if params[:created_at]

    purchase.save
  end

  def total
    return @total if defined? @total

    @total = @purchases.map { |p| p.price }.sum
  end

  def display_month
    return params[:month] if params[:month]

    t = Time.now
    "#{t.year}-#{'%02d' % t.month}"
  end

  helper_method :total
  helper_method :display_month
end
