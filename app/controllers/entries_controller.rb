class EntriesController < ApplicationController
  def show
    @entry = Purchase.find(params[:id])
  end

  def update
    @entry = Purchase.find(params[:id])

    @entry.store = params[:store]
    @entry.price = params[:price]
    @entry.category = params[:category]

    @entry.save

    redirect_to purchases_show_path(category: @entry.category)
  end

  def delete
    Purchase.find(params[:id]).delete
  end
end
