class DefaultCategoryController < ApplicationController
  def index
    @default_categories = DefaultCategory.order(:category).all
  end
end
