class Purchase < ApplicationRecord
  before_create do
    default_category = DefaultCategory.find_by(store:)
    if category.nil? || category == 'None'
      self.category = default_category.category unless default_category.nil?
    elsif default_category.nil?
      DefaultCategory.new(store:, category:).save
    end
  end
end
