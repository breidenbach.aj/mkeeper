# Pin npm packages by running ./bin/importmap

pin 'application', preload: true

pin 'chartkick', to: 'chartkick.js'
pin 'Chart.bundle', to: 'Chart.bundle.js'
pin "alpinejs", to: "https://ga.jspm.io/npm:alpinejs@3.12.2/dist/module.esm.js"
