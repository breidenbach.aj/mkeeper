Rails.application.routes.draw do
  get 'purchase', to: 'purchase#index', as: 'purchase_index'
  post 'purchase', to: 'purchase#create', as: 'purchase_create'
  get 'purchase/:category', to: 'purchase#show', as: 'purchase_show'

  get 'purchases', to: 'purchase#index', as: 'purchases_index'
  post 'purchases', to: 'purchase#create', as: 'purchases_create'
  get 'purchases/:category', to: 'purchase#show', as: 'purchases_show'

  get 'entries/:id', to: 'entries#show', as: 'entries_show'
  post 'entries/:id', to: 'entries#update', as: 'entries_update'
  delete 'entries/:id', to: 'entries#delete', as: 'entries_delete'

  get 'default-categories', to: 'default_category#index', as: 'default_category_index'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
